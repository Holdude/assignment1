# Komputer Store

## Description

Komputer Store is a store where you can browse and buy computers. Here users can make money to buy computers by either working or taking up loans.


## Wireframe
The wireframe used is the one which came with the assignment

## Usage
There are 4 different modules to this application.

### Bank
"Balance" shows the total money that can be used towards buying a computer.

"Loan" only shows up after a loan has been taken up, and disappears when it is fully paid off. It shows how much debt the user is currently in.

"Get loan" button lets the user try to get a loan.

"Pay loan" button only shows up after a loan has been taken up, and disappears when it is fully paid off. It will pay out the users full salary towards the debt, any remaining money are then transfered into the balance account.

### Work
"Pay" is the users current salary and is not included in your balance.

"Bank" button takes your salary and puts it in the balance account. If you have a active loan 10% of the pay will go towards downpayment.

"Work" button adds 100 kr every click to your salary.

### Laptops
The pull down menu shows all available computers, selecting one will show the specs of the computer below.

### Computer view
Shows information about the selected computer. Here the user can see the description, price, name and an image of the pc. By clicking the "Buy" button the user can buy the computer if they can afford it.

## Used

CSS, Javascript.

Used Noroff's computer api for the computer data: https://noroff-komputer-store-api.herokuapp.com/computers

## Creators
Erlend Hollund
