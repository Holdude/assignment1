// DOM
let getBalance = document.getElementById("balance");
let getWorkbalance = document.getElementById("workBalance");
let getLoanBalance = document.getElementById("loanSum"); 
let loanArt = document.getElementById("loandArt");
let selectBar = document.getElementById("barSelect");
let buttonPayloan = document.getElementById("payLoanButton");

// Bank 
let balanceSum = 500;
let loanedSum = 0;
let workedSum = 0;

// Array
let computersArray = [];

// Sets balance
getBalance.innerHTML = balanceSum + " kr";
getLoanBalance.innerHTML = loanedSum+" kr";
getWorkbalance.innerHTML = workedSum+ " kr";

// Fetching data from api

fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
    .then(response => response.json()
    .then(data => computersArray = data)
    .then(computersArray => addComputer(computersArray)));

    

const addComputer = (computersArray) => {

    computersArray.forEach(pc => {
        const computer = document.createElement("option");
        computer.value = pc.id;
        computer.appendChild(document.createTextNode(pc.title));
        selectBar.appendChild(computer);
    })

    // Runs first time
    selectComputer();

}

// Function for selecting computer to view
function selectComputer(){

    // Resets features
    document.getElementById("feat").innerHTML = "";

    computersArray.forEach(pc =>{
        if(pc.id == barSelect.value){

            // Change data in html page
            // h1
            document.getElementById("comHeader").innerHTML = pc.title;
            // image
            document.getElementById("comImage").src = "https://noroff-komputer-store-api.herokuapp.com/"+pc.image;
            // desc
            document.getElementById("comDesc").innerHTML = pc.description
            // price
            document.getElementById("comPrice").innerHTML = pc.price+" kr";

            // Outputs features
            pc.specs.forEach(spec =>{
                document.getElementById("feat").innerHTML +="<p>"+ spec+"</p>"

            })
        }
    })


}

// Onclick getloanButton
function getLoan(){
    let amount =prompt("Enter ammount to loan:");

    // Checks for loan approval
    // if lower than 2x of current balance
    
    if(amount <= balanceSum*2){

        // Test if loan already exists
        if( loanedSum >=1){
        } else{

            // Add money to loanedSum
            loanedSum = amount;

            // Calculates new balance
            balanceSum = parseInt(balanceSum) + parseInt(loanedSum);

            // Changes on webpage
            loanArt.style.visibility = "visible";
            buttonPayloan.style.visibility = "visible";
            updatePage();

        }

    }

}

// Function for working
function getWork(){
    // Adds 100 to work balance
    workedSum += 100;
    getWorkbalance.innerHTML = workedSum+ " kr";
}



// Function for paying back loan directly from work sum
function payLoan(){
    // Locac variable
    let localLoansum = parseInt(workedSum)-parseInt(loanedSum);
    
    // Checks if there is money left after loan is payed
    if(localLoansum>0){


        balanceSum += localLoansum;

        loanedSum = 0;
        workedSum = 0;

    } else{
        // change loan value
        loanedSum -= workedSum;
        workedSum =0;

    }

    updatePage();

}

// Function for utdating webpage with data
function updatePage(){
    getLoanBalance.innerHTML = loanedSum +" kr";
    getWorkbalance.innerHTML = workedSum + " kr";
    getBalance.innerHTML = balanceSum + " kr";

}

// Function for banking money/paying loan
function bankMoney(){

    
    let payOutput = workedSum;
    // Part of work sum for downpayment
    let tenProcent = 10*workedSum/100;

    // Checks if there is a current loan
    if(loanedSum>0){
        //Checks if the current debt is larger than 10% of current pay
        if(loanedSum>= tenProcent){
            // Is smaler then current debt


            payOutput -= tenProcent;
            loanedSum -= tenProcent;

            
        } else{

            payOutput -= tenProcent;

            // Local loan var
            let locLoan = loanedSum;
            
            // Flips from negative to posivite to add tp balance again
            locLoan = Math.abs(locLoan-tenProcent);

            // Ads the money left after paying loan back into payout
            payOutput += locLoan;

            // Resets loan to 0
            loanedSum =0;

        }
    }

    // Pays out 
    balanceSum += payOutput;
    workedSum =0;
    updatePage();


        // Check if loan is 0
        if(loanedSum ==0){
            buttonPayloan.style.visibility = "hidden";
            loanArt.style.visibility = "hidden";
        }
}


// Fetching from api
async function fetchData(){

    fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
    // parse to JSON obj
    .then(function(response){
        return response.json()
    })

    .then(function(posts){

        // Add to local array
        posts.forEach(element => {

            computersArray.push(element);
        });

    })

    // Error handling
    .catch(function(error){

    })

    
}

// Function for buying the computer
function buyComputer(){

    computersArray.forEach(element =>{
        // Select correct computer obj
        if(element.id == selectBar.value){
            // Check if user can afford
            if(balanceSum >= element.price){
                // Change balance
                balanceSum -= element.price;
                alert("Computer bought");

                
            } else{
                // Can't afford
                alert("Can't afford this computer");
            }
        }
    })

    getBalance.innerHTML = balanceSum;
}
